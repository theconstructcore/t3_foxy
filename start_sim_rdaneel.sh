#! /usr/bin/env bash

# We set up the environment for ROS2
. /usr/share/gazebo/setup.sh
. /home/tgrip/TheConstruct/ros_2_playground/t3/install/setup.bash
export GAZEBO_RESOURCE_PATH=/home/tgrip/TheConstruct/ros_2_playground/t3/src/t3_foxy/turtlebot3_simulations/turtlebot3_gazebo:${GAZEBO_RESOURCE_PATH}
export GAZEBO_MODEL_PATH=/home/tgrip/TheConstruct/ros_2_playground/t3/src/t3_foxy/turtlebot3_simulations/turtlebot3_gazebo/models:${GAZEBO_MODEL_PATH}
export TURTLEBOT3_MODEL=waffle
ros2 launch turtlebot3_gazebo turtlebot3_tc_world.launch.py
