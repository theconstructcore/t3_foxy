#! /usr/bin/env bash

# We remove a folder that otherwise gives issues in ROS2 launches
sudo rm -r ~/.ros

# We set up the environment for ROS2
. /usr/share/gazebo/setup.sh
export GAZEBO_RESOURCE_PATH=/home/tgrip/TheConstruct/ros_playgound/turtlebot3_foxy_ws/src/t3_foxy/turtlebot3_simulations/turtlebot3_gazebo:${GAZEBO_RESOURCE_PATH}
export GAZEBO_MODEL_PATH=/home/tgrip/TheConstruct/ros_playgound/turtlebot3_foxy_ws/src/t3_foxy/turtlebot3_simulations/turtlebot3_gazebo/models:${GAZEBO_MODEL_PATH}
export TURTLEBOT3_MODEL=waffle
ros2 launch turtlebot3_gazebo turtlebot3_world.launch.py
